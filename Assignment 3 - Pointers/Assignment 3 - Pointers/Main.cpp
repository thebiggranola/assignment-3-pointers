
// Assignment 3 - Pointers
// <Your Name>


#include <iostream>
#include <conio.h>



using namespace std;



void SwapIntegers(int *first , int *second) {
	int *pF = first;
	int *pS = second;
	
	int swap = *pF;
	*pF = *pS;
	*pS = swap;

}

// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	(void)_getch();
	return 0;
}
